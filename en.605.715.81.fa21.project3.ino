// Basing Programs for Motor with uno kit here https://www.arduinoplatform.com/driving-a-brushed-dc-motor-with-arduino/

int motorPin = 12;
int irLedPin = 13;
unsigned long timeold;
volatile int rpmcount;
unsigned int rpm;
void setup() {
  Serial.begin(9600);
  Serial.println("TIME, RPM");
  attachInterrupt(digitalPinToInterrupt(2), rpmCounter, FALLING); 
  pinMode(irLedPin, OUTPUT);
  digitalWrite(irLedPin, HIGH);
  pinMode(motorPin, OUTPUT ); 
  rpmcount = 0;
  rpm = 0;
  logRpmTime();
  analogWrite(motorPin, 200);
}
 
void loop() { 
    delay(1000);
    noInterrupts();
    rpm = 20*1000/(millis() - timeold)*rpmcount;
    timeold = millis();
    logRpmTime();
    rpmcount = 0;
    rpm = 0;
    interrupts();
}
  
void rpmCounter() {
  rpmcount++;
}

void logRpmTime() {
    Serial.print(timeold); 
    Serial.print(" , ");
    Serial.println(rpm);
}
